pub struct CreateContainerOptions {
    image: Image,
}

#[derive(Debug, PartialEq)]
pub struct Image(String);

impl Image {
    pub fn new(name: &str) -> Self {
        Image(name.into())
    }
}

#[derive(Debug, PartialEq)]
pub struct ExitCode(pub i32);
