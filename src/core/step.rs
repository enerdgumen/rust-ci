use crate::docker::{ExitCode, Image};
use vec1::Vec1;

#[derive(Debug, PartialEq)]
pub struct Step {
    pub name: Name,
    pub image: Image,
    pub commands: Vec1<String>,
    pub result: Option<Result>,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, PartialOrd, Ord)]
pub struct Name(String);

impl Name {
    pub fn new(name: &str) -> Self {
        Name(name.into())
    }
}

#[derive(Debug, PartialEq)]
pub enum Result {
    Failed(ExitCode),
    Succeeded,
}

impl From<ExitCode> for Result {
    fn from(code: ExitCode) -> Self {
        if let ExitCode(0) = code {
            Result::Succeeded
        } else {
            Result::Failed(code)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn container_exit_code_to_step_result() {
        assert_eq!(Result::Succeeded, ExitCode(0).into());
        assert_eq!(Result::Failed(ExitCode(2)), ExitCode(2).into());
    }
}
