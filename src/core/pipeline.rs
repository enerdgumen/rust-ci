use crate::core::step;
use std::collections::BTreeMap;

#[derive(Debug, PartialEq)]
pub struct Pipeline {
    pub steps: BTreeMap<step::Name, step::Step>,
}
