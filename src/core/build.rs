use crate::core::pipeline::Pipeline;
use crate::core::step;
use crate::docker::ExitCode;
use either::Either;

#[derive(Debug, PartialEq)]
pub struct Build {
    pub pipeline: Pipeline,
    pub state: State,
}

impl Build {
    pub fn progress(mut self) {
        use State::*;
        match self.state {
            Ready => match self.next_step() {
                Either::Left(step) => {
                    self.state = Running(RunningState {
                        step_name: step.name.clone(),
                    })
                }
                Either::Right(result) => self.state = Finished(result),
            },
            Running(state) => {
                let step = self.pipeline.steps.get_mut(&state.step_name).unwrap();
                let exit_code = ExitCode(0);
                step.result = Some(exit_code.into());
            }
            Finished(_) => todo!(),
        }
    }

    fn next_step(&self) -> Either<&step::Step, Result> {
        for step in self.pipeline.steps.values() {
            if step.result.is_none() {
                return Either::Left(step);
            }
            if let Some(step::Result::Failed(_)) = step.result {
                return Either::Right(Result::Failed);
            }
        }
        return Either::Right(Result::Succeeded);
    }
}

#[derive(Debug, PartialEq)]
pub enum State {
    Ready,
    Running(RunningState),
    Finished(Result),
}

#[derive(Debug, PartialEq)]
pub struct RunningState {
    step_name: step::Name,
}

#[derive(Debug, PartialEq)]
pub enum Result {
    Succeeded,
    Failed,
}
